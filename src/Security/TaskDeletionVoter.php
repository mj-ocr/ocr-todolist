<?php


namespace App\Security;


use App\Entity\Task;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class TaskDeletionVoter extends Voter
{

    /**
     * @inheritDoc
     */
    protected function supports(string $attribute, $subject)
    {
        return $attribute === "IS_TASK_OWNER";
    }

    /**
     * @param string $attribute
     * @param Task $subject
     * @param TokenInterface $token
     * @return int|void
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token)
    {
        return $subject->getOwner() === $token->getUser();
    }

}
