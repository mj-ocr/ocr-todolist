<?php


namespace App\Security;


use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class IsTheUserVoter extends Voter
{

    /**
     * @inheritDoc
     */
    protected function supports(string $attribute, $subject)
    {
        return $attribute === "IS_THE_USER";
    }

    /**
     * @inheritDoc
     */
    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token)
    {
        return $subject === $token->getUser() && $token->getUser() instanceof UserInterface;
    }
}
