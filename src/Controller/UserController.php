<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Form\UserTypeForAdmin;
use App\Form\UserUpdateType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserController extends AbstractController
{
    /**
     * @Route("/users", name="user_list")
     * @IsGranted("ROLE_ADMIN")
     */
    public function listAction()
    {
        return $this->render('user/list.html.twig', ['users' => $this->getDoctrine()->getRepository('App:User')->findAll()]);
    }

    /**
     * @Route("/users/create", name="user_create")
     */
    public function createAction(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $password = $passwordEncoder->encodePassword($user, $user->getPassword());
            $user->setPassword($password);

            $em->persist($user);
            $em->flush();

            $this->addFlash('success', "L'utilisateur a bien été ajouté.");

            return $this->isGranted('ROLE_ADMIN') ? $this->redirectToRoute('user_list') : $this->redirectToRoute('login');
        }

        return $this->render('user/create.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/users/{id}/edit", name="user_edit")
     * @param User $updatedUser
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return RedirectResponse|Response
     * @Security("is_granted('ROLE_ADMIN') or is_granted('IS_THE_USER', updatedUser)")
     */
    public function editAction(User $updatedUser, Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $currentPassword = $updatedUser->getPassword();

        $form = $this->isGranted("IS_THE_USER", $updatedUser)
            ? $this->createForm(UserUpdateType::class, $updatedUser)
            : $this->createForm(UserTypeForAdmin::class, $updatedUser);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if ($currentPassword !== $updatedUser->getPassword()) {
                $password = is_null($updatedUser->getPassword())
                    ? $currentPassword
                    : $passwordEncoder->encodePassword($updatedUser, $updatedUser->getPassword());
                $updatedUser->setPassword($password);
            }

            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', "L'utilisateur a bien été modifié");

            return $this->isGranted('ROLE_ADMIN') ? $this->redirectToRoute('user_list') : $this->redirectToRoute('homepage');
        }

        return $this->render('user/edit.html.twig', ['form' => $form->createView(), 'user' => $updatedUser]);
    }
}
