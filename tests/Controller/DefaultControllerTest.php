<?php

namespace App\Tests\Controller;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    public function testIndexAnonymous()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        self::assertResponseRedirects('/login', 302);
    }

    public function testIndexUser()
    {
        $client = static::createClient();
        $userRepository = static::$container->get(UserRepository::class);

        $testUser = $userRepository->findOneBy(['username' => 'user_1']);

        $client->loginUser($testUser);

        $crawler = $client->request('GET', '/');

        self::assertSelectorTextSame("h1","Bienvenue sur Todo List, l'application vous permettant de gérer l'ensemble de vos tâches sans effort !");
    }
}
