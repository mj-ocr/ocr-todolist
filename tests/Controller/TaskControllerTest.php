<?php


namespace App\Tests\Controller;


use App\Entity\Task;
use App\Entity\User;
use App\Repository\TaskRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TaskControllerTest extends WebTestCase
{
    /**
     * @dataProvider provideUrls
     */
    public function testForbiddenAccess(string $url) {
        $client = static::createClient();
        $crawler = $client->request('GET', str_replace('{id}', $this->getTaskId(), $url));
        self::assertResponseRedirects('/login', 302);
    }

    /**
     * @dataProvider provideViews
     */
    public function testViewsAccess(string $url) {
        $client = static::createClient();

        $client->loginUser($this->getUser('user_1'));

        $crawler = $client->request('GET', str_replace('{id}', $this->getTaskId(), $url));
        self::assertResponseIsSuccessful();
    }

    public function testTaskToggle() {
        $client = static::createClient();
        $client->loginUser($this->getUser('user_1'));

        $task = $this->getTask();
        $isDone = $task->isDone();

        $sentences = [
            'Marquer comme faite',
            'Marquer non terminée',
        ];

        $client->request('GET', '/tasks/' . $task->getId() . '/toggle');
        self::assertResponseRedirects('/tasks', 302);
        $crawler = $client->followRedirect();
        $text = $crawler
            ->filter('#task-' . $task->getId())
            ->children()
            ->filter('button')
            ->text();
        self::assertStringContainsString($sentences[(int) !$isDone], $text);

        $client->request('GET', '/tasks/' . $task->getId() . '/toggle');
        self::assertResponseRedirects('/tasks', 302);
        $crawler = $client->followRedirect();
        $text = $crawler
            ->filter('#task-' . $task->getId())
            ->children()
            ->filter('button')
            ->text();
        self::assertStringContainsString($sentences[(int) $isDone], $text);
    }

    /**
     * @dataProvider provideUsers
     */
    public function testTaskDelete($user): void
    {
        $client = self::createClient();

        $user = $this->getUser($user);
        $client->loginUser($user);

        $task1 = $this->getUserTaskId($user);
        $task2 = $this->getUserTaskId($this->getUser('anonymous'));

        $client->request('GET', 'tasks/' . $task1 . '/delete');
        self::assertResponseRedirects('/tasks', 302);
        $crawler = $client->followRedirect();
        $flashMessage = $crawler->filter('.alert.alert-success')->first()->text();
        self::assertStringContainsString('La tâche a bien été supprimée.', $flashMessage);

        $client->request('GET', 'tasks/' . $task2 . '/delete');
        if ($user->getUsername() === 'admin_1') {
            self::assertResponseRedirects('/tasks', 302);
            $crawler = $client->followRedirect();
            $flashMessage = $crawler->filter('.alert.alert-success')->first()->text();
            self::assertStringContainsString('La tâche a bien été supprimée.', $flashMessage);
        } else {
            self::assertResponseStatusCodeSame(403);
        }
    }

    public function testTaskCreation(): void
    {
        $client = self::createClient();
        $client->loginUser($this->getUser('user_1'));
        $client->request('GET', '/tasks/create');

        $client->submitForm('Ajouter', [
            'task[title]' => 'Test',
            'task[content]' => 'Une nouvelle tâche',
        ]);

        self::assertResponseRedirects('/tasks', 302);
        $crawler = $client->followRedirect();
        $flashMessage = $crawler->filter('.alert.alert-success')->first()->text();
        self::assertStringContainsString('La tâche a été bien été ajoutée.', $flashMessage);
    }

    public function provideUrls(): array
    {
        return [
            ['/tasks'],
            ['/tasks/create'],
            ['/tasks/{id}/edit'],
            ['/tasks/{id}/toggle'],
            ['/tasks/{id}/delete'],
        ];
    }

    public function provideViews(): array
    {
        return [
            ['/tasks'],
            ['/tasks/create'],
            ['/tasks/{id}/edit'],
        ];
    }

    public function provideUsers(): array
    {
        return [
            ['admin_1'],
            ['user_1'],
        ];
    }

    private function getTaskRepository(): TaskRepository
    {
        return self::$container->get(TaskRepository::class);
    }

    private function getTask(): Task
    {
        $tasks = $this->getTaskRepository()->findAll();
        return $tasks[array_rand($tasks)];
    }

    private function getTaskId(): int
    {
        return $this->getTask()->getId();
    }

    private function getUserTaskId($user): int
    {
        return $this->getTaskRepository()->findOneBy(['owner' => $user])->getId();
    }

    private function getUser($username): ?User
    {
        $userRepository = self::$container->get(UserRepository::class);
        return $userRepository->findOneBy(['username' => $username]);
    }
}
