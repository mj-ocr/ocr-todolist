<?php


namespace App\Tests\Controller;


use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserControllerTest extends WebTestCase
{
    public function testUserCreation(): void
    {
        $client = self::createClient();
        $userNumber = random_int(0, 999999);
        $client->request('GET', '/users/create');
        $client->submitForm('Ajouter', [
            'user[username]' => 'TestUser_' . $userNumber,
            'user[password][first]' => 'Password',
            'user[password][second]' => 'Password',
            'user[email]' => 'testuser' . $userNumber . '@email.com',
        ]);

        self::assertResponseRedirects('/login', 302);
        $crawler = $client->followRedirect();
        $flashMessage = $crawler->filter('.alert.alert-success')->first()->text();
        self::assertStringContainsString('L\'utilisateur a bien été ajouté.', $flashMessage);
    }

    /**
     * @dataProvider provideUsers
     */
    public function testUserList($user):void
    {
        $client = self::createClient();

        if ($user) {
            $client->loginUser(self::getUser($user));
        }

        $client->request('GET', '/users');

        if (!$user) {
            self::assertResponseRedirects('/login', 302);
        } elseif ($user === 'user_1') {
            self::assertResponseStatusCodeSame(403);
        } else {
            self::assertResponseIsSuccessful();
        }
    }

    /**
     * @dataProvider provideUsers
     */
    public function testUserUpdate($user):void
    {
        $client = self::createClient();

        if ($user) {
            $client->loginUser(self::getUser($user));
        }

        $client->request('GET', '/users/' . self::getUser('user_2')->getId() . '/edit' );

        if (!$user) {
            self::assertResponseRedirects('/login', 302);
        } elseif ($user === 'user_1') {
            self::assertResponseStatusCodeSame(403);
        } else {
            self::assertResponseIsSuccessful();
        }
    }

    public function provideUsers()
    {
        return [
            [''],
            [ 'user_1'],
            ['admin_1'],
        ];
    }

    public static function getUser($username): ?User
    {
        $userRepository = self::$container->get(UserRepository::class);
        return $userRepository->findOneBy(['username' => $username]);
    }
}
