<?php


namespace App\Tests\Controller;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\Tests\Controller\UserControllerTest;

class SecurityControllerTest extends WebTestCase
{
    public function testLogin(): void
    {
        $client = self::createClient();
        $client->request('GET', '/login');
        $client->submitForm('Se connecter', [
            'username' => 'user_1',
            'password' => $_ENV['TEST_USER_SECRET'],
        ]);

        self::assertResponseRedirects('/tasks', 302);
    }

    public function testLogout(): void
    {
        $client = self::createClient();
        $client->loginUser(UserControllerTest::getUser('user_1'));
        $client->request('GET', '/logout');

        $client->followRedirect();
        self::assertResponseRedirects('/login', 302);
    }
}
