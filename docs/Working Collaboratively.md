# Working Collaboratively

## How to process an issue

When starting working on this project, you must follow a set of rules in order to work collaboratively in an efficient
and qualitative way.

1. You must `git clone` the project from [our Gitlab repository](https://gitlab.com/mj-ocr/ocr-todolist).
2. Each new feature or bug fix must have a related issue. 
3. When you start working on an issue, you assign yourself to this issue and push the button `Create a Merge Request`. This will:
    - create a new branch on which you will work (don't forget to `git checkout` before starting to code)
    - create a merge request from your branch to the master branch
4. `git commit` regularly. In other words, do not make one big commit when your feature is ready but proceed by step. Each step being one commit with a **clear commit message**
5. `git push` regularly. This ensures you have a backup of your work on our gitlab repository.
6. A new feature **comes with new phpunit tests**. This is part of your work to write test and this is not an option. 
The minimum code coverage for the app is 70%, please ensure this is the case. If this is not the case, write new tests.
6. Once you're done with your issue `git push` everything to the repository. 
Then, ask one of your colleague to proceed to a **code review**. As for tests, this is not optional. 
7. Once your code has been reviewed and, your merge request validated, you can actually `git merge` your branch into master.
8. You can now start a new issue.

## Important folders

This project is based on the framework Symfony 5.1+. It means that you must respect a certain organization in the way
you structure your files and folders (MVC organization). Below is the list of important folders to keep in mind:

### `/src`

This is where you write your code. Particularly:
 
- the `Entity` folder contains the list of... entities of the project (e.g. Task, User).
- controllers are... in the `Controller` folder
- `Repository` is where you put files used to retrieve data from the database.
- `Security` is where you put file used for authentication or access control.
- `Form` is where you define the forms used in the app.

### `/tests`

This is the folder where you write unit and functional tests. The tool we use for this is PHPUnit. Is this folder too 
you must respect a certain structure. You must actually mirror the structure of the `/src` folder and add a Test suffix 
to filenames. For instance : `/tests/Controller/TaskControllerTest.php`.

### Other folders

Some other folders to keep in mind:

- `/config`: this is where all config files are stored. Generally written in yaml.
- `/fixtures`: in yaml as well, this is where you define fixtures (fake data for test purpose)
- `/migrations`: when you modify or create an Entity, you must generally change the structure of the database. Migrations are used for this.
- `/public`: mainly used to store static assets as css, js or images.
- `/templates`: this is where you write views using Twig language.
