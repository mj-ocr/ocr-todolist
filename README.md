# ToDoList

## Presentation

This is a project made for an Openclassrooms project. The objective was to work on and improve an existing project.

Quality of code has been analysed by [Codacy](https://app.codacy.com/gl/mj-ocr/ocr-todolist/dashboard?branch=master).

## Requirements

To be able to test the project in a local environment, you will need :

* PHP 7.1+
* composer
* MySQL / MariaDB
* [Symfony-CLI](https://symfony.com/download)

## Installation on a local environment

1. Clone the project : `git clone git@gitlab.com:mj-ocr/ocr-todolist.git`
2. Go into the project repository : `cd ocr-todolist`
3. Install dependencies : `composer install`
4. Create you .env.local file : `touch .env.local`
5. Add your own [DATABASE_URL](https://symfony.com/doc/current/doctrine.html#configuring-the-database) environment variable into the .env.local file.
6. Setup the dabatase : `php bin/console doctrine:database:create`
7. Load the fixtures: `php bin/console hautelook:fixtures:load`
9. Run symfony local server : `symfony server:start`
10. Open your browser, go to http://localhost:8000/docs and enjoy!

If you used the fixtures provided, credentials are :

* user_1, user_2, user_X / Password
* admin_1, admin_2, admin_X / Password

## Run tests

To run tests, you need to:

1. Initialize phpunit the first time : `php bin/phpunit`
2. Add your own [DATABASE_URL](https://symfony.com/doc/current/doctrine.html#configuring-the-database) environment variable into the .env.test.local file.
2. Run tests using the same command.

## Work on the projects

Please read the [docs](docs)! 
